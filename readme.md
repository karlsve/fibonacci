Fibonacci
=========

A simple Assembly Programm to calculate the n-th Fibonacci Number limited by 32-bit UInt (Register usage).

To edit run ./edit
To compile run ./compile
To edit and compile right after run ./editc
