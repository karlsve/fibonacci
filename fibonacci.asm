global main
extern printf

section .data
	message: db 'F(%d) = %d', 10, 0		;Output message
	errormessage: db 'ERROR: n < 0', 10, 0	;Error message

section .text				;TODO error if F(n) > 32-Bit UInteger Range
main:
	mov ecx, 10			;Init ecx with n
	push ecx			;Push ecx onto stack to get it back later
	mov eax, 0			;Init eax as 0
	mov ebx, 1			;Init ebx as 1
	cmp ecx, 0			;Compare ecx to 0
	je output			;Jump directly to output if ecx == 0
	jl error			;Jump to error if ecx < 0
fibonacci:
	mov edx, ebx			;Move ebx to edx
	add ebx, eax			;Add eax to ebx to get the new number
	mov eax, edx			;Get lower number from edx as eax
	loop fibonacci			;Repeat if not 0
output:
	pop ecx				;Get ecx from stack to fix order
	push eax			;Push eax to stack
	push ecx			;Push ecx to stack
	push message			;Push message to stack
	call printf			;Print output message
	add esp, 12			;Fix stack
	mov eax, 0			;eax return value
	ret				;Exit since stack is balanced
error:
	push errormessage		;Push error message to stack
	call printf			;Print error message
	add esp, 8			;Fix stack
	mov eax, 1			;eax return value
	ret				;Exit since stack is balanced
